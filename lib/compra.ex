defmodule Compra do

  defstruct [:email, :valor]

  def calcular([], _) do
    {:error, "Nenhum e-mail foi informado"}
  end

  def calcular(emails, itens) do
    total_compra = total(itens)
    valor_por_email = div(total_compra, Enum.count(emails))
    resto_a_pagar = rem(total_compra, Enum.count(emails))

    compras = for email <- emails do
      %Compra{email: email, valor: valor_por_email}
    end

    ultima_compra = List.last(compras)
    nova_ultima_compra = %Compra{ultima_compra | valor: ultima_compra.valor + resto_a_pagar}
    nova_compras = List.replace_at(compras, Enum.count(compras) - 1, nova_ultima_compra)

    {:ok, nova_compras}
  end

  def total(itens) do
    total(itens, 0)
  end

  defp total([item | restante], total_compra) do
    total_item = Item.total(item)
    total(restante, total_compra + total_item)
  end

  defp total(_, total_compra) do
    total_compra
  end
end
