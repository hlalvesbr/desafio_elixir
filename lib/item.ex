defmodule Item do

  defstruct [:indice, :descricao, :preco_unitario, :quantidade]

  def total(item) do
    item.preco_unitario * item.quantidade
  end
end
