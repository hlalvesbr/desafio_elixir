defmodule DesafioElixir do
  @moduledoc """
  Documentation for `DesafioElixir`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> DesafioElixir.hello()
      :world

  """
  def hello do
    :world
  end
end
