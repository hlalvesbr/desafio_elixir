defmodule CompraTest do
  use ExUnit.Case

  test "calcula total da compra" do
    item1 = %Item{indice: 1, descricao: "Produto 1", preco_unitario: 1000, quantidade: 3}
    item2 = %Item{indice: 2, descricao: "Produto 2", preco_unitario: 2000, quantidade: 1}
    total = Compra.total([item1, item2])
    assert total == 5000
  end

  test "divide total da compra entre compradores" do
    item1 = %Item{indice: 1, descricao: "Produto 1", preco_unitario: 1000, quantidade: 3}
    item2 = %Item{indice: 2, descricao: "Produto 2", preco_unitario: 2000, quantidade: 1}
    item3 = %Item{indice: 3, descricao: "Produto 3", preco_unitario: 5000, quantidade: 1}
    {:ok, compradores} = Compra.calcular(["email1", "email2", "email3"], [item1, item2, item3])
    [comprador | _] = compradores
    assert Enum.count(compradores) == 3
    assert comprador.valor == 3333
    assert List.last(compradores).valor == 3334
  end
end
