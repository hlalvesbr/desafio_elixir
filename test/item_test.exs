defmodule ItemTest do
  use ExUnit.Case

  test "calcula total do item" do
    item = %Item{indice: 1, descricao: "Produto", preco_unitario: 1000, quantidade: 3}
    total = Item.total(item)
    assert total == 3000
  end
end
